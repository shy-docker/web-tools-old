const fs = require('fs')
const shell = require('shelljs')
const { nginx_conf_dir } = require('../..')
const [ vhost_dir, ssl_dir, rewrite_dir ] = [ '/vhost', '/ssl','/rewrite' ].map(f=>nginx_conf_dir+f)
/**@param {string} host */
let format_vhostname = host=>host.replace(/\.conf(|\.stop)$/,'')
let vhost_arr = shell.ls(vhost_dir).map(format_vhostname)
let rewrite_arr = shell.ls(rewrite_dir).map(f=>f.replace(/\.conf$/,''))
module.exports = {
  vhost_arr, rewrite_arr,
  vhost_dir, ssl_dir, rewrite_dir,
  format_vhostname,
  /**
   * @param {string} host 
   */
  checkout_host(host){
    return /.+\..+/.test(host)
  },
  /**
   * @param {string} host
   * @param {string[]} used_list 不可使用的域名
   */
  async get_usable_hostname(host,used_list=[]){
    let inquirer = require('inquirer')
    for (let a;;) {
      if( require('./').checkout_host(host) && !vhost_arr.includes(host)){
        a && shell.echo(`the hostname [${host}] will be used`)
        break
      }
      a = 1
      let message = used_list.includes(host)
      ?`the hostname already has been used, please input anthor (${host}): `
      :`the hostname can't be used, please input anthor (${host}): ` 
      host = (await inquirer.prompt({ name:'val', message, type:'input', })).val || host
    }
    return host
  },
  /**
   * 
   * @param {string} host 
   * @returns {string[]}
   */
  get_aliases_name(host){
    if(!vhost_arr.includes(host))return []
    let match = shell.cat(`${vhost_dir}/${host}*`).match(/server_name(.+)\;/)
    if(!match)return []
    return match[1].split(/\s+/).filter(x=>x)
  },
}