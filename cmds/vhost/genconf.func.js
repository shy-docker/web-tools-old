/**
 * 生成 nginx 配置
 * @param {vhost.config} argv
 */
exports.genconf = (argv)=>`
server {
  listen 80;
${ !argv.ssl ? `` : `
# -------- ssl start --------------
  listen 443 ssl http2;
  ssl_certificate /etc/nginx/conf.d/ssl/${argv.host}.crt;
  ssl_certificate_key /etc/nginx/conf.d/ssl/${argv.host}.key;
  ssl_protocols TLSv1 TLSv1.1 TLSv1.2;
  ssl_ciphers EECDH+CHACHA20:EECDH+AES128:RSA+AES128:EECDH+AES256:RSA+AES256:EECDH+3DES:RSA+3DES:!MD5;
  ssl_prefer_server_ciphers on;
  ssl_session_timeout 10m;
  ssl_session_cache builtin:1000 shared:SSL:10m;
  ssl_buffer_size 1400;
  add_header Strict-Transport-Security max-age=15768000;
  ssl_stapling on;
  ssl_stapling_verify on;
  if ($ssl_protocol = "") { return 301 https://$host$request_uri; }
# ---------- ssl end ---------------
`}
  server_name ${argv.host} ${argv.aliases?argv.aliases.join(' '):''};
  access_log /www/logs/${argv.host}_nginx.log combined;
  index index.html index.htm index.php;
  root /www/${argv.host};
${ !argv.rewrite ? `` : `
# -------- rewrite rule start --------------
  include /etc/nginx/conf.d/rewrite/${argv.rewrite}.conf;
# -------- rewrite rule end   --------------
`}
  #error_page 404 /404.html;
  #error_page 502 /502.html;

  location ~ \.php {
    fastcgi_pass web_php:9000;
    fastcgi_index index.php;
    include fastcgi_params;
    set $real_script_name $fastcgi_script_name;
    if ($fastcgi_script_name ~ "^(.+?\.php)(/.+)$") {
      set $real_script_name $1;
      #set $path_info $2;
    }
    fastcgi_param SCRIPT_FILENAME $document_root$real_script_name;
    fastcgi_param SCRIPT_NAME $real_script_name;
    #fastcgi_param PATH_INFO $path_info;
  }

  location ~ .*\.(gif|jpg|jpeg|png|bmp|swf|flv|mp4|ico)$ {
    expires 30d;
    access_log off;
  }
  location ~ .*\.(js|css)?$ {
    expires 7d;
    access_log off;
  }
  location ~ /\.ht {
    deny all;
  }
}
`