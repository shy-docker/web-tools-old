const yargs = require('yargs')
const shell = require('shelljs')
module.exports = {
  command:'stop',
  desc: 'stop web services',
  /**
   * @param {yargs} yargs 
   */
  builder(yargs){
    // return yargs.commandDir('init')
  },
  /**
   * 
   * @param {*} argv 
   */
  handler(argv){
    shell.exec('docker stack down web')
  },
}