const yargs = require('yargs')
const shell = require('shelljs')
const fs = require('fs')
module.exports = {
  command:'service [template]',
  desc: 'add web service template and start it',
  usage: [
    'usage: `web add service shy-docker/web-template`',
    'the service template will be download form the gitlab.com master repository ',
  ].join('\r\n'),
  /**
   * @param {yargs} yargs 
   */
  builder(yargs){
    return yargs.default('template','shy-docker/web-template')
  },
  /**
   * 
   * @param {{template:string}} argv 
   */ 
  handler(argv){
    if(fs.existsSync('/web/www')){
      shell.echo('服务模板只能添加一次')
      shell.exit(1)
      return
    }
    // download service template
    let storage_dir = '/tmp/web-tools-template'
    shell.exec([
      `mkdir -p ${storage_dir}`,
      `curl https://gitlab.com/${argv.template}/repository/master/archive.tar.gz | tar -xz -C ${storage_dir}`,
      `mv ${storage_dir}/*/* /web`,
    ].join(' && '))
    shell.exec(`rm -rf ${storage_dir}`)
    // start nginx and php service
    shell.exec('web np update')
  },
}