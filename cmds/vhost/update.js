/// <reference path="./vhost.type.d.ts" />
const yargs = require('yargs')
const shell = require('shelljs')
const { 
  vhost_arr, rewrite_arr, 
  ssl_dir, vhost_dir, 
  get_usable_hostname, get_aliases_name,checkout_host 
} = require('./')
const ssl_info = '/C=CN/ST=Shanghai/L=Shanghai/O=Example Inc./OU=IT Dept.'
const fs = require('fs')
const inquirer = require('inquirer')
/**
 * @param {string} host 
 */
function match_aliases(host){

}
module.exports = {
  command:'update',
  desc: 'update [host] config',
  /**
   * @param {yargs} yargs 
   */
  builder(yargs){
    const { ui } = yargs.argv
    return yargs
  },
  /**
   * @param {vhost.config} argv 
   */
  async coerce(argv){
    argv = await require('./genconf').coerce(argv)
    return argv
  },
  /**
   * @param {vhost.config} argv 
   */
  async handler(argv){
    argv = await this.coerce(argv)
    return 
    let host = argv.host
    if(argv.ssl){
      shell.exec(`openssl req -new -newkey rsa:2048 -sha256 -nodes -out ${ssl_dir}/${argv.host}.csr -keyout ${ssl_dir}/${argv.host}.key -subj "${ssl_info}/CN=${argv.host}" > /dev/null 2>&1`)
      let filename_prefix = `${ssl_dir}/${argv.host}`
      shell.exec(`openssl x509 -req -days 36500 -sha256 -in ${filename_prefix}.csr -signkey ${filename_prefix}.key -out ${filename_prefix}.crt > /dev/null 2>&1`)
    }
  }
}