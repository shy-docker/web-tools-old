# 下载node
NODE_VERSION="node-v8.9.4-linux-x64"
curl https://nodejs.org/dist/v8.9.4/$NODE_VERSION.tar.xz | tar xvJf -

# 把node加入环境变量
echo "export PATH=\"\$PATH:/web/$NODE_VERSION/bin\"" >> ~/.bashrc
source ~/.bashrc