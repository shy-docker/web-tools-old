const yargs = require('yargs')
const shell = require('shelljs')
module.exports = {
  command: 'docker',
  desc: 'install docker',
  /**
   * @param {yargs} yargs
   */
  builder(yargs){
    return yargs.option('on',{ default:'' })
  },
  /**
   * @param {{on:string}} argv 
   */
  handler(argv){
    /**@type {(cmd:string)=>string} */
    const ssh_wrap = argv.on?cmd=>`${argv.on} '${cmd}'`:cmd=>cmd
    /**@param {string} cmd*/
    const run = cmd=>shell.exec(ssh_wrap(cmd))
    console.log(ssh_wrap('which docker'))
    if(run('which docker').code === 0){
      shell.echo('docker already has been installed')
      return
    }
    let linux_release = run('cat /etc/issue').toString()
    switch(true){
      case /centos.+7/i.test(linux_release):
        run('curl https://gitlab.com/shy-docker/init/raw/master/install-scripts/docker-centos7.sh | bash')
        break;
      case /ubuntu/i.test(linux_release):
        run('curl https://gitlab.com/shy-docker/init/raw/master/install-scripts/docker-ubuntu.sh | bash')
        break;
      case !!shell.error():
        shell.exit(1)
        break
      default:
        shell.echo('web-tools 尚未支持该系统,系统信息如上')
        shell.exit(1)
        break;
    }
  },
}