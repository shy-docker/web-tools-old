const yargs = require('yargs')
module.exports = {
  command:'sv <command>',
  aliases: ['services'],
  desc: 'web service command',
  /**
   * @param {yargs} yargs 
   */
  builder(yargs){
    return yargs.commandDir('services')
  },
  /**
   * 
   * @param {*} argv 
   */
  handler(argv){
  },
}