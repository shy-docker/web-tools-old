module.exports = {
  command:'open [host]',
  desc:'open the stopped nginx [host] service',
  builder:require('./stop').builder,
  handler:require('./stop').handler,
}