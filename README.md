## install

```sh
curl https://gitlab.com/shy-docker/init/raw/master/web-tools-install.sh | bash
```

## init

```sh
web init install
```

## 集群状态查看

1. 首先开启代理 `ssh -D 1091 root@manager_node`

2. 然后浏览器通过代理访问 <http://visualizer.shynome.com/> 查看节点信息
