const yargs = require('yargs')
module.exports = {
  command : 'vhost <command>',
  aliases: ['vh'],
  desc : 'init service',
  /**
   * 
   * @param {yargs} yargs 
   */
  builder(yargs){
    return yargs.commandDir('vhost')
  },
  /**
   * 
   * @param {*} argv 
   */
  handler(argv){

  },
}