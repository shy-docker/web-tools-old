/// <reference path="./vhost.type.d.ts" />
const yargs = require('yargs')
const inquirer = require('inquirer')

const shell = require('shelljs')
const { genconf } = require('./genconf.func')
const { 
  rewrite_arr, vhost_arr,
  checkout_host, get_aliases_name, get_usable_hostname
} = require('./')

const _exports = module.exports = {
  command:'genconf',
  aliases: ['gen'],
  desc:'gen nginx host conf',
  /**@param {yargs} yargs */
  builder(yargs){
    return yargs
    .option('ssl',{
      default:false,
      desc:'是否开启ssl',
    })
    .option('rewrite',{
      default:'thinkphp',
      desc:'使用哪条重写规则',
    })
    .option('aliases',{
      default:[],
      desc:'域名别名',
      type:'array',
    })
  },
  /**
   * @typedef { vhost.config & {ui?:boolean} } argv
   */
  /**@param {vhost.config} argv  */
  async coerce(argv){

    argv.host = await get_usable_hostname(argv.host)
    
    /**
     * @typedef {argv & {ask_more_aliases:boolean}} question_argv
     */
    /**
     * @typedef {Object} question
     * @prop { (argv:question_argv)=>boolean } [when]
     * @prop { keyof question_argv } name
     * @prop { string } message
     * @prop { 'list'|'confirm'|'string'|'input' } type
     * @prop { any[] } [choices]
     * @prop { any } [default]
     * @prop { any } [defaultIfEmpty]
     * @prop { (input:any)=>boolean } [validate]
     * @prop { (input:any)=>any } [filter]
     * @prop { (input:any)=>any } [transformer]
     */
    let questions = /**@type {question[]} */([])
    // ssl
    questions.push({ 
      name:'ssl',
      defaultIfEmpty:argv.ssl,
      type:'confirm', 
      message:'是否开启ssl (开启后如果不是泛域名证书的话,不要使用别名,因为别名访问的话会提示https会不安全)', 
    })
    // rewirte rule
    questions.push({ 
      name:'rewrite',
      message:'选择rewirte rule', 
      defaultIfEmpty:argv.rewrite,
      type:'list', 
      choices:rewrite_arr,
    })
    // aliases
    questions.push(
      {
        name:'ask_more_aliases',
        type:'confirm',
        defaultIfEmpty:false,
        message:`已有别名: ${JSON.stringify(argv.aliases)} . '是否添加更多别名? `,
      },
      {
        name:'aliases',
        default: argv.aliases,
        when(argv){ return argv.ask_more_aliases },
        type:'input',
        message:'输入别名, 多个用空格分割:',
        /**
         * @param {string} val 
         */
        filter(val){
          console.log(arguments)
          return val.split(/\s+/)
        },
      }
    )

    /**@type {question_argv} */
    let _argv = /**@type {*}*/(await inquirer.prompt(/**@type {*}*/(questions)))

    argv = Object.assign(argv,_argv)

    return argv
  },
  /**@param {argv} argv */
  async handler(argv){
    if(argv.ui){
      argv = await _exports.coerce(argv)
    }
    shell.echo(genconf(argv))
  }  
}

