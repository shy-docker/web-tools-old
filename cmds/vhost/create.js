/// <reference path="./vhost.type.d.ts" />
const yargs = require('yargs')
const { vhost_arr, rewrite_arr, ssl_dir, vhost_dir, get_usable_hostname  } = require('./')
const inquirer = require('inquirer')

module.exports = {
  command:'create [host]',
  desc:'create a web services',
  /**
   * @param {yargs} yargs
   */
  builder(yargs){
    return yargs
  },
  /**
   * @param {vhost.config} argv
   */
  async coerce(argv){
    argv = await require('./genconf').coerce(argv)
    return argv
  },
  handler:require('./update').handler,
}