
# 所有的文件都会放在这里
mkdir -p /web && cd /web 

# 安装 node
if !(hash node 2>/dev/null); then
  curl https://gitlab.com/shy-docker/web-tools/raw/dev/install-scripts/node.sh | bash
fi

# 安装 web-tools
if !(hash web 2>/dev/null); then
  npm i -g gitlab:shy-docker/web-tools
fi

# 初始化web服务
web services init

