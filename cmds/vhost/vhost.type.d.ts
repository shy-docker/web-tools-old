export as namespace vhost

export type config = {
  host:string,
  ssl:boolean,
  rewrite:'thinkphp',
  aliases:string[],
}