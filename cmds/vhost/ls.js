const yargs = require('yargs')
const shell = require('shelljs')
const fs = require('fs')
const { vhost_arr, rewrite_dir, ssl_dir, vhost_dir  } = require('./')
const rewrite_regx = new RegExp(`${rewrite_dir}/(.+)\\.conf`)
module.exports = {
  command:'list',
  aliases:['ls'],
  desc: 'vhost list',
  /**
   * @param {yargs} yargs 
   */
  builder(yargs){
    // return yargs.commandDir('init') 
  },
  /**
   * 
   * @param {any} argv 
   */
  handler(argv){
    const ui_w = 60
    const ui = require('cliui')({ width:ui_w })

    const head = ['hostname','status','ssl','rewrite rule']
    /**@param {string} text */
    const col = text =>({ text, width:ui_w/head.length })

    ui.div({ text:'vhost list:', padding:[2,0,1,0] })
    ui.div(...head.map(col))

    vhost_arr
    .map(hostname=>{
      
      let conf_path = `${vhost_dir}/${hostname}.conf`
      
      let ssl = fs.existsSync(ssl_dir+`/${hostname}.csr`)?'on':'off'

      let rewrite_match = shell.cat(`${conf_path}*`).match(rewrite_regx)
      let rewrite = rewrite_match?rewrite_match[1]:'off'
      
      let status = fs.existsSync(`${conf_path}.stop`)?'stop':'open'
      
      return [hostname,status,ssl,rewrite]
    })
    .forEach(arr=>{
      ui.div(...arr.map(col))
    })

    ui.div({ padding:[0,0,1,0], text:'' })

    console.log(ui.toString())
  },
}