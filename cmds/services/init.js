const yargs = require('yargs')
const shell = require('shelljs')
const fs = require('fs')
module.exports = {
  command:'init',
  desc: 'init web services',
  /**
   * @param {yargs} yargs 
   */
  builder(yargs){
    // return yargs.commandDir('init') 
  },
  /**
   * 
   * @param {*} argv 
   */
  handler(argv){
    shell.exec('web add service && web sv update')
  },
}