const yargs = require('yargs')
module.exports = {
  command:'add <command>',
  desc: 'add module',
  /**
   * @param {yargs} yargs 
   */
  builder(yargs){
    return yargs.commandDir('addons')
  },
  /**
   * 
   * @param {*} argv 
   */
  handler(argv){
  },
}