const yargs = require('yargs')
const shell = require('shelljs')
const fs =require('fs')
const { vhost_arr, vhost_dir } = require('./')
const usage = require('cliui')()
const inquirer = require('inquirer')
usage.div({
  text:'web vhost stop [host] \r\n web vhost open [host]',
  padding:[1,2,1,2]
})
module.exports = {
  command:'stop [host]',
  desc:'stop the nginx [host] service',
  /**
   * @param {yargs} yargs 
   */
  builder(yargs){
    return yargs
    .usage(usage.toString())
  },
  /**
   * @typedef {{host:string,_:string[]}} argv 
   */
  /**
   * @param {argv} argv
   */
  async coerce(argv){
    let list = /**@type {string[]} */(null)
      , opreating = /**@type {string} */(argv._[1])
    switch(opreating){
      case 'stop':
        list = shell.ls(`${vhost_dir}/*.conf`)
        break
      case 'open':
        list = shell.ls(`${vhost_dir}/*.conf.stop`)
        break
    }
    list = list.map(require('./').format_vhostname).map(f=>f.replace(vhost_dir+'/',''))
    if(!list.includes(argv.host)){
      let { host } = await inquirer.prompt({
        name:'host',
        message:`select the vhostname which you will ${opreating} `,
        type:'list',
        choices:list,
      })
      argv.host = host
    }
    return argv
  },
  /**
   * @param {argv} argv 
   */
  async handler(argv){
    argv = await require('./stop').coerce(argv)

    let conf_path = `${vhost_dir}/${argv.host}.conf`
    let conf_stop_path = conf_path+'.stop'

    switch(argv._[1]){
      case 'stop':
        shell.mv(conf_path,conf_stop_path)
        break
      case 'open':
        shell.mv(conf_stop_path,conf_path,)
        break
    }
    

  },
}